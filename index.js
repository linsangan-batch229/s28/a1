//S28 activity

// insertOne
db.hotel.insertOne({
    name : "single",
    accomodate : 2,
    price : 1000,
    description : "A simple room with all basic necessities",
    room_Available : "10",
    isAvailable : false   
})



// insertMany
db.hotel.insertMany([
    {
        name : "double",
        accomodate : 3,
        price : 2000,
        description : "A room fit for a small family going on a vacation",
        room_Available : 5,
        isAvailable : false   
    },
    {
        name : "queen",
        accomodate : 4,
        price : 4000,
        description : "A room with a queen sized bed perfect for a simple gateway",
        room_Available : 15,
        isAvailable : false   
    }
   
])


//findOne = name: "double"
db.hotel.find({name: "double"})


//updateOne = name : queen , room_Available: 0
db.hotel.updateOne({name: "queen"}, {$set : {room_Available: 0}})

//deleteMany = room_Available : 0
db.hotel.deleteMany({room_Available: 0})